package storage

import (
	"Task_01/book"
	"errors"
)

type Storage interface {
	GetBook(id string) (book.Book, error)
	AddBook(book.Book, string)
}

type MapStorage struct {
	BooksMap map[string]book.Book
}

func NewMapStorage() *MapStorage {
	return &MapStorage{make(map[string]book.Book)}
}

func (storage *MapStorage) GetBook(id string) (book.Book, error) {
	if _, ok := storage.BooksMap[id]; !ok {
		return book.Book{}, errors.New("element does not exist")
	}
	return storage.BooksMap[id], nil

}

func (storage *MapStorage) AddBook(addedBook book.Book, hash string) {
	storage.BooksMap[hash] = addedBook
}

type SliceStorage struct {
	BooksSlice []book.Book
	HashToId   map[string]int
}

func NewSliceStorage() *SliceStorage {
	return &SliceStorage{make([]book.Book, 0), make(map[string]int)}
}

func (storage *SliceStorage) GetBook(hash string) (book.Book, error) {
	return storage.BooksSlice[storage.HashToId[hash]], nil
}

func (storage *SliceStorage) AddBook(addedBook book.Book, hash string) {
	if _, ok := storage.HashToId[hash]; ok {
		return
	}
	storage.HashToId[hash] = len(storage.BooksSlice)
	storage.BooksSlice = append(storage.BooksSlice, addedBook)
}
