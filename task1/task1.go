package main

import (
	"Task_01/book"
	"Task_01/library"
	"Task_01/storage"
	"fmt"
)

func main() {
	MapLib := library.NewLibrary(storage.NewMapStorage())
	books := []book.Book{
		book.Book{Title: "Test1", Author: "Author1"},
		book.Book{Title: "Test2", Author: "Author2"},
		book.Book{Title: "Test3", Author: "Author3"},
		book.Book{Title: "Test4", Author: "Author4"},
		book.Book{Title: "Test5", Author: "Author5"},
	}
	for _, myBook := range books {
		MapLib.AddBook(myBook)
	}

	fmt.Println(MapLib.FindBook("Test1"))
	fmt.Println(MapLib.FindBook("Test2"))

	SliceLib := library.NewLibrary(storage.NewSliceStorage())
	for _, myBook := range books {
		SliceLib.AddBook(myBook)
	}

	fmt.Println(MapLib.FindBook("Test4"))
	fmt.Println(MapLib.FindBook("Test3"))

}
