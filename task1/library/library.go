package library

import (
	"Task_01/storage"
	"crypto/sha256"
)
import "Task_01/book"

type Library struct {
	storage storage.Storage
}

func NewLibrary(libStorage storage.Storage) *Library {
	return &Library{storage: libStorage}
}

func (lib *Library) HashBook(title string) string {
	hsh := sha256.Sum256([]byte(title))
	str := string(hsh[:])
	return str
}

func (lib *Library) AddBook(addedBook book.Book) {
	lib.storage.AddBook(addedBook, lib.HashBook(addedBook.Title))
}

func (lib *Library) FindBook(title string) (book.Book, error) {
	return lib.storage.GetBook(lib.HashBook(title))
}
