package task_handlers

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"task2/internal/config"
	"time"
)

type MyHandler struct {
	cfg *config.Config
}

type decodeInput struct {
	Str string `json:"inputString"`
}

type decodeOutput struct {
	Str string `json:"outputString"`
}

func NewHandler(cfg *config.Config) *MyHandler {
	return &MyHandler{
		cfg,
	}
}

func (handler *MyHandler) GetVersion(writer http.ResponseWriter, request *http.Request) {
	fmt.Println("/version ...")
	writer.WriteHeader(http.StatusOK)
	_, _ = writer.Write([]byte(handler.cfg.Version))
	return
}

func (handler *MyHandler) PostDecode(writer http.ResponseWriter, request *http.Request) {
	fmt.Println("/decode ...")
	requestData := decodeInput{}
	decoder := json.NewDecoder(request.Body)

	defer func() {
		err := request.Body.Close()
		if err != nil {
			writer.WriteHeader(http.StatusInternalServerError)
		}
	}()

	err := decoder.Decode(&requestData)
	if err != nil {
		http.Error(writer, err.Error(), http.StatusBadRequest)
		return
	}

	decoded, err := Base64Decode(requestData.Str)
	if err != nil {
		http.Error(writer, err.Error(), http.StatusBadRequest)
		return
	}

	response, err := json.Marshal(decodeOutput{decoded})
	if err != nil {
		http.Error(writer, err.Error(), http.StatusInternalServerError)
		return
	}
	writer.WriteHeader(http.StatusOK)
	_, _ = writer.Write(response)
}

func (handler *MyHandler) GetHard(writer http.ResponseWriter, request *http.Request) {
	fmt.Println("Hard-op ...")
	timeout := time.Duration((rand.Int()%11)+10) * time.Second
	finished := make(chan bool)

	go func() {
		time.Sleep(timeout)
		finished <- true
	}()

	select {
	case <-finished:
		fmt.Println("Hard-op ended")
		if rand.Int()%3 == 0 {
			writer.WriteHeader(http.StatusOK)
		} else {
			writer.WriteHeader(http.StatusGatewayTimeout)
		}
	case <-request.Context().Done():
		writer.WriteHeader(http.StatusInternalServerError)
	}

}

func Base64Decode(str string) (string, error) {
	data, err := base64.StdEncoding.DecodeString(str)
	if err != nil {
		return "", err
	}
	return string(data), nil
}
