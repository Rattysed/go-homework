package server

import (
	"context"
	"fmt"
	"net/http"
	"task2/internal/config"
	task_handlers "task2/internal/handlers"
)

type Server struct {
	cfg    *config.Config
	server *http.Server

	taskHandler *task_handlers.MyHandler
}

func NewServer(cfg *config.Config) *Server {
	taskHandler := task_handlers.NewHandler(cfg)

	addres := cfg.Address + ":" + cfg.Port

	server := Server{
		cfg:         cfg,
		taskHandler: taskHandler,
		server: &http.Server{
			Addr:    addres,
			Handler: initApi(cfg),
		},
	}
	return &server
}

func (s *Server) Run() {
	go func() {
		err := s.server.ListenAndServe()
		if err != nil {
			fmt.Println(err)
		}
	}()
}

func (s *Server) Stop(ctx context.Context) {
	fmt.Println(s.server.Shutdown(ctx))
}

func initApi(cfg *config.Config) http.Handler {
	mux := http.NewServeMux()
	taskHandlers := task_handlers.NewHandler(cfg)

	mux.HandleFunc("/version", taskHandlers.GetVersion)
	mux.HandleFunc("/decode", taskHandlers.PostDecode)
	mux.HandleFunc("/hard-op", taskHandlers.GetHard)

	return mux
}
