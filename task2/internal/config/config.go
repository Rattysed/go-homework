package config

import (
	"encoding/json"
	"os"
)

type Config struct {
	Address string `json:"address"`
	Port    string `json:"port"`
	Version string `json:"version"`
}

func NewConfig(path string) (*Config, error) {
	data, err := os.ReadFile(path)
	if err != nil {
		return nil, err
	}

	var cfg Config
	err = json.Unmarshal(data, &cfg)
	if err != nil {
		return nil, err
	}
	return &cfg, nil
}
