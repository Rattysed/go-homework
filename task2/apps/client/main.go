package main

import (
	"bytes"
	"context"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"task2/internal/config"
	"time"
)

func main() {
	var cfgPath string

	flag.StringVar(&cfgPath, "cfg", "./cfg/client.json", "set cfg path")

	cfg, err := config.NewConfig(cfgPath)
	if err != nil {
		log.Fatal("Lol, something's wrong with your config:", err)
	}

	defaultURL := "http://" + cfg.Address + ":" + cfg.Port

	// Get version
	response, err := http.Get(defaultURL + "/version")

	if err != nil {
		fmt.Println("Smth wrong with getting version:", err)
	}

	body, err := io.ReadAll(response.Body)
	if err != nil {
		fmt.Println("Deserialization failed:", err)
	}
	defer func() {
		_ = response.Body.Close()
	}()
	fmt.Println(string(body))

	// Post

	coded := `{"inputString": "0JLRgNC+0LTQtSDQutCw0Log0L7QvdC+INGA0LDQsdC+0YLQsNC10YI="}`
	response, err = http.Post(defaultURL+"/decode", "application/json", bytes.NewBuffer([]byte(coded)))
	if err != nil {
		log.Fatal("Smth wrong with getting version:", err)
	}

	body, err = io.ReadAll(response.Body)
	if err != nil {
		log.Fatal("Deserialization failed:", err)
	}

	fmt.Println(string(body))

	// Hard-op

	timeout := time.Duration(15) * time.Second
	ctx, stop := context.WithTimeout(context.Background(), timeout)
	defer stop()

	respCh := make(chan *http.Response)
	errCh := make(chan error)

	go func() {
		resp, err := http.Get(defaultURL + "/hard-op")
		if err != nil {
			errCh <- err
			return
		}
		errCh <- nil
		respCh <- resp
	}()

	select {
	case err := <-errCh:
		if err != nil {
			fmt.Println("Long task failed:", err)
			return
		}
	case <-ctx.Done():
		fmt.Println("Timed out")
		return
	}

	resp := <-respCh
	fmt.Println(resp.StatusCode)
}
