package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"
	"task2/internal/config"
	"task2/internal/server"
	"time"
)

func main() {
	var cfgPath string
	flag.StringVar(&cfgPath, "cfg", "./cfg/server.json", "set cfg path")
	flag.Parse()
	fmt.Println("Starting...")

	cfg, err := config.NewConfig(cfgPath)
	if err != nil {
		log.Fatal("Lol, something's wrong with your config:", err)
	}

	serv := server.NewServer(cfg)
	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer stop()

	defer func() {
		v := recover()

		if v != nil {
			ctx, stop := context.WithTimeout(context.Background(), 3*time.Second)
			defer stop()
			serv.Stop(ctx)
			<-ctx.Done()
			fmt.Println(v)
			os.Exit(1)
		}
	}()

	serv.Run()
	fmt.Println("Server Started")

	<-ctx.Done()
	ctx, stop = context.WithTimeout(context.Background(), 3*time.Second)
	defer stop()
	serv.Stop(ctx)
	<-ctx.Done()
}
